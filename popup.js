document.addEventListener('DOMContentLoaded', function() {
    var checkPageButton = document.getElementById('report');
    checkPageButton.addEventListener('click', function() {
  
      chrome.tabs.getSelected(null, function(tab) {
        var newURL = 'https://opencell.assembla.com/p/tickets/list?filter[ticket_report]=u1776773';
        chrome.tabs.create({ url: newURL });
      });
    }, false);
  }, false);
  
document.addEventListener('DOMContentLoaded', function() {
    var checkPageButton = document.getElementById('time');
    checkPageButton.addEventListener('click', function() {
  
      chrome.tabs.getSelected(null, function(tab) {
        var newURL = 'https://opencell.assembla.com/p/timesheets/analysis';
        chrome.tabs.create({ url: newURL });
      });
    }, false);
  }, false);
 

document.addEventListener('DOMContentLoaded', function() {
    var trackTime = document.getElementById('track-time');
    trackTime.addEventListener('click', function() {
  
      chrome.tabs.getSelected(null, function(tab) {
        var newURL = 'https://app.assembla.com/user/tasks';
        chrome.tabs.create({ url: newURL });
      });
    }, false);
  }, false);
  
document.addEventListener('DOMContentLoaded', function() {
    var vsa = document.getElementById('vsa');
    vsa.addEventListener('click', function() {
  
      chrome.tabs.getSelected(null, function(tab) {
        var newURL = 'https://opencell.vsactivity.com/o_homepage/';
        chrome.tabs.create({ url: newURL });
      });
    }, false);
  }, false);
  
document.addEventListener('DOMContentLoaded', function() {
    var inputSearch = document.getElementById('search');
	inputSearch.focus();
    inputSearch.addEventListener('keyup', function(e) {
		if (e.keyCode === 13) {
			var newURL = 'https://opencell.assembla.com/spaces/meveo/tickets/'+ encodeURIComponent(inputSearch.value);
			chrome.tabs.create({ url: newURL });
		}
		else{
			console.log(e.keyCode);
		}
    }, false);
  }, false);

  /*
var links={
	'report':'https://opencell.assembla.com/p/tickets/list?filter[ticket_report]=u1776773',
	'time': 'https://opencell.assembla.com/p/timesheets/analysis',
	'track-time':'https://app.assembla.com/user/tasks'
}	

for (var key in links) {
  document.addEventListener('DOMContentLoaded', function() {
    var checkPageButton = document.getElementById(key);
    checkPageButton.addEventListener('click', function() {
      chrome.tabs.getSelected(null, function(tab) {
		var newURL = links[key];
        chrome.tabs.create({ url: newURL });
      });
    }, false);
  }, false);
}
*/